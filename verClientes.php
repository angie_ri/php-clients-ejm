<!doctype html>
<html lang="es">
  <?php  
     include("head.php");

     include("clases/cliente.php");
     include("clases/controlDB.php");   
     include("clases/myQuery.php");
     include("clases/pagination.php");

      /*Incializo la clase Pagination para conseguir los clientes a mostrar*/
     $pagination = new Pagination('php_puro');
     $countPages =$pagination->pages('clientes',10); //cantidad de paginas a mostrar

  if(  $countPages > 0) {
     $pagAux = (int) $_GET['page']; //posición en la se encuentra el paginador
     /* si la posición es inconsistente  entonces vuelve a la primera pagina*/ 
     if( $pagAux > $countPages  || $pagAux < 1){
       header('Location: verClientes.php?page=1');
     }

      $init = $pagAux-1; //posición en que debe iniciar la busqueda sql de clientes


      $allclientes = $pagination->getQuery($init, 10, 'clientes'); //clientes buscados por cierta cantidad
  }
    ?>

  <body>

    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
      <h5 class="my-0 mr-md-auto font-weight-normal">ArSoft Develop</h5>
    
    </div>

    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">Clientes</h1>
      <p class="lead">PHP-POO</p>
    </div>
  
    <div class="container verClientes" >

      <?php
        /*Incializo la clase Clientes para conseguir los clientes a mostrar
        $clientes = new Cliente();
        $allclientes= $clientes->getClientes();
       */
      if(  $countPages  > 0) {
          ?>

          <table class="table table-bordered display" id="table_cli" data-page-length='5'>
              <thead>
              <tr>
                  <th scope="col">#</th>
                  <th scope="col">Nombres</th>
                  <th scope="col">Apellidos</th>
                  <th scope="col">DNI</th>
                  <th scope="col">Email</th>
                  <th scope="col">Teléfono</th>
                  <th scope="col">Acción</th>
              </tr>
              </thead>
              <tbody>

              <?php

              foreach ($allclientes as $cliente) {

                  echo '<tr>
            <th scope="row">' . $cliente['id'] . '</th>
            <td>' . $cliente["nombres"] . '</td>
            <td>' . $cliente["apellidos"] . '</td>
            <td>' . $cliente["dni"] . '</td>
            <td>' . $cliente["email"] . '</td>
            <td>' . $cliente["telefono"] . '</td>
            <td><a href="client.php?id=' . $cliente['id'] . '" ><button type="button" class="btn btn-outline-dark">Editar</button></a></td>
          </tr>';
              }
              ?>

              </tbody>
          </table>

          <nav aria-label="Page navigation example">
              <ul class="pagination  justify-content-center">
                  <li class="page-item <?php echo $pagAux <= 1 ? 'disabled' : '' ?> ">
                      <a class="page-link " href="verClientes.php?page=<?php echo $pagAux - 1 ?>" aria-label="Previous">
                          <span aria-hidden="true">&laquo;</span>
                      </a>
                  </li>
                  <?php
                  $active = '';

                  if ($pagAux >= $countPages + 1) {
                      $active = 'active';
                  }

                  for ($i = 1; $i < $countPages + 1; $i++):
                      echo '<li class="page-item"><a class="page-link' . $active . '"  href="verClientes.php?page=' . $i . '">' . $i . '</a></li>';
                  endfor


                  ?>
                  <li class="page-item <?php echo $pagAux >= $countPages ? 'disabled' : '' ?> ">
                      <a class="page-link " href="verClientes.php?page=<?php echo $pagAux + 1 ?>" aria-label="Next">
                          <span aria-hidden="true">&raquo;</span>
                      </a>
                  </li>
              </ul>
          </nav>
          <?php
      }else{
        ?>
          <hr>
          <div class="">
              <p href="index.php" class=" text-decoration-none text-left">No se encontraron resultados.</p>

          </div>

          <hr>
          <?php
      }
        ?>
      <div class="links">
        <a href="index.php" class="font-italic text-decoration-none text-left"><- Home</a>
     
      </div>
      <?php  
       include("footer.php");
       ?>

    </div>

      <?php  
       include("scripts.php");
        ?>
  </body>
</html>