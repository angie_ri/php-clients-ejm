<!doctype html>
<html lang="en">
  <?php  
     include("head.php");
     include("clases/cliente.php");
     include("clases/controlDB.php");   
     include("clases/myQuery.php");
    /*Incializo la clase Clientes para conseguir los clientes a mostrar*/
    $clientes = new Cliente('php_puro');

    $id =$_GET["id"];
    $clientById= $clientes->getClient($id);
    //var_dump(gettype($clientById),$cliente['id']);

   
  ?>

  <body>

    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
      <h5 class="my-0 mr-md-auto font-weight-normal">ArSoft Develop</h5>
     <!-- <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="#">Features</a>
        <a class="p-2 text-dark" href="#">Enterprise</a>
        <a class="p-2 text-dark" href="#">Support</a>
        <a class="p-2 text-dark" href="#">Pricing</a>
      </nav>
      <a class="btn btn-outline-primary" href="#">Sign up</a>-->
    </div>

    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">Clientes</h1>
      <p class="lead">PHP-POO</p>
    </div>

    <div class="container" >
    	<?php 

    	if(isset($_GET['succes'])){
    		//$estado =$suceccs;
    		if($_GET['succes'] == 'true'){
    			echo '<div class="alert alert-success" role="alert" >
						  Los datos del cliente han sido editado!
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>';

    		}elseif ($_GET['succes'] == 'false'){
    			echo'<div class="alert alert-warning" role="alert">
					  	Error al editar Cliente.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
					  	</button>
					  </div>';
    		}
    	}
    	
    	

    	foreach($clientById as  $valor) {
	    
    		echo 
	     	'<form method="Post" action="accions.php?id='. $id .'">
		     	<input type="hidden" name="form" value="editClient">
		     	<div class="input-group mb-3">
				  <div class="input-group-prepend">
				    <span class="input-group-text" id="basic-addon1">Nombres</span>
				  </div>
				  <input type="text" class="form-control" placeholder="Nombres" aria-label="Username" aria-describedby="basic-addon1" value="'.$valor["nombres"].'" name="nombres">
				</div>

				<div class="input-group mb-3">
				  <div class="input-group-prepend">
				    <span class="input-group-text" id="basic-addon1">Apellidos</span>
				  </div>
				  <input type="text" class="form-control" placeholder="Apellidos" aria-label="Username" aria-describedby="basic-addon1" value="'.$valor["apellidos"].'" name="apellidos">
				</div>
				<div class="input-group mb-3">
				  <div class="input-group-prepend">
				    <span class="input-group-text" id="basic-addon1">DNI</span>
				  </div>
				  <input type="text" class="form-control" placeholder="Apellidos" aria-label="Username" aria-describedby="basic-addon1" value="'.$valor["dni"].'" name="dni">
				</div>
				<div class="input-group mb-3">
				  <div class="input-group-prepend">
				    <span class="input-group-text" id="basic-addon1">Email</span>
				  </div>
				  <input type="text" class="form-control" placeholder="Apellidos" aria-label="Username" aria-describedby="basic-addon1" value="'.$valor["email"].'" name="email">
				</div>

				<div class="input-group mb-3">
				  <div class="input-group-prepend">
				    <span class="input-group-text" id="basic-addon1">Telefono</span>
				  </div>
				  <input type="text" class="form-control" placeholder="Apellidos" aria-label="Username" aria-describedby="basic-addon1" value="'.$valor["telefono"].'" name="telefono">
				</div>
				<button type="submit" class="btn btn-outline-dark btn-block">Enviar</button>
	        </form>';

		}
		?>
		<div class="links">
			<a href="index.php" class="font-italic text-decoration-none text-left"><- Home</a>
			<a href="verClientes.php" class="font-italic text-decoration-none text-right" id="linkClient">Ver Clientes -></a>
		</div>
    <?php
     
     include("footer.php");

      ?>

	</div>

   <?php  
     include("scripts.php");
      ?>

  </body>
</html>