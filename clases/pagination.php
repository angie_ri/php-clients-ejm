<?php

use function Couchbase\defaultDecoder;

class Pagination{

    private $db_conexion;
    private $db_query;
    private $countQuery;
    
/**
 * recibe en el constructor la base con la que se conectará 
 */
    public function __construct($base){

        $this->db_conexion = new ControlDB($base);
        $this->db_query = new MyQuery;
       
      
    }

    
/** retorna cantidad de paginas requeridas por el limite de consultas requerida */
    public function pages($table,$limit){

        $sql = $this->db_query->selectAllQuery($table);
        $conexion =$this->db_conexion->conectarDB();
        $query = mysqli_query($conexion,$sql) or die ("Error al conseguir '.$table.'");

        if(mysqli_affected_rows($this->db_conexion->conectarDB())){

          return false;
    
        }else{

          $arrayQuery = mysqli_fetch_all($query);
          $this->countQuery =count($arrayQuery);
          $pages = $this->countQuery/$limit;
          $pages = ceil($pages);
          $this->db_conexion->cerrarConexion();

          return $pages;
          
        } 

    }

    /**Pre:$pagNow,$count,$table parametros para conseguir clientes
     * Post:retorna una cantidad de objeto clientes 
     */
    public function getQuery($pagNow,$count,$table){

        $init = $pagNow * $count;
        $conexion =$this->db_conexion->conectarDB();
        $sql = $this->db_query->limitQuery($table,$init,$count);
        $query = mysqli_query($conexion,$sql) or die ("Error al conseguir '.$table.'");

        if(mysqli_affected_rows($this->db_conexion->conectarDB())){

			return false;

		}else{
			$this->db_conexion->cerrarConexion();
		
			return $query;
		} 
    }
}

?>