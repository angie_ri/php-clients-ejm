<?php 
class ControlDB{
		private $conexionDB ;
		private $baseDeDatos;
	
	/**
	 * inicializa con el constructor ingresando la base de datos que va a trabajar
	 */
	function __construct($baseDeDatos){
		$this->baseDeDatos = $baseDeDatos;
		
		}
	/**
	 * Conecta con la base de datos 
	 */
	public function conectarDB(){

		try{
	//Declaramos las variables
			$host ="localhost";
			$db_name="$this->baseDeDatos";
			$user="root";
			$pass="";
		//Conexion
	   
			$this->conexionDB = mysqli_connect($host,$user,$pass,$db_name) or die ("Error en la conexion");
			
			if($this->conexionDB){
				//var_dump($this->conexionDB);
				return $this->conexionDB;
			}
		
		}catch(Exception $ex){
			
			throw $ex ;
		}
		
	}

	/**
	 * cierra la conexion con la BD
	 */
	public function cerrarConexion(){

		$db =$this->conectarDB();
		mysqli_close($db);

	}
}

?>