<?php 

class Cliente{	

	private $db_cliente;
	private $query;
	private $nombres ;
	private $apellidos ;
	private $dni;
	private $email;
	private $estructura;
	private $sql;

	function __construct($base){

		$this->db_cliente = new ControlDB($base);
		$this->query = new MyQuery;
		
		
	}

	/**
	 * en parametro recibe una array de los del cliente a ingresar,
	 * luego de registrar se muestra el formulario desde cero
	 */
	public function registrarCliente($array){

		$this->estructura = $array;
	
		$this->sql = $this->query->insertarQuery($this->estructura,"clientes");

		$insert=mysqli_query($this->db_cliente->conectarDB(),$this->sql) or die ("Error al insertar cliente");

		if(mysqli_affected_rows($this->db_cliente->conectarDB())){

			return false;

		}else{
			$this->db_cliente->cerrarConexion();
		
			return  header("Location: verClientes.php");
		} 

	}

	/**
	 * retorna los clientes que se encuentran registrados
	 */
	public function getClientes(){

		$conexion =$this->db_cliente->conectarDB();
		$selectAll = $this->query->selectAllQuery("clientes");
		$clientes = mysqli_query($conexion,$selectAll) or die ("Error al conseguir cliente");

		if(mysqli_affected_rows($this->db_cliente->conectarDB())){

			return false;

		}else{
			$this->db_cliente->cerrarConexion();
		
			return $clientes;
		} 
		
	}

	/**
	 * $id es el id del cliente a conseguir
	 * retorna los datos del cliente a mostrar y o editar
	 */
	public function getClient($id){
		
		$conexion =$this->db_cliente->conectarDB();
		$selectById = $this->query->selectById("clientes",$id);
		$cliente = mysqli_query($conexion,$selectById) or die ("Error al conseguir cliente");

		if(mysqli_affected_rows($this->db_cliente->conectarDB())){

			return false;

		}else{
			$this->db_cliente->cerrarConexion();
		
			return $cliente;
		}
	}

	/**
	 * array,id son variables que recibe para poder actualizar los datos del cliente
	 */
	public function updateClient($array,$id){

		$conexion =$this->db_cliente->conectarDB();
		//$this->estructura = $array;
	
		$idClient = $id;
		$this->sql = $this->query->updateQuery("clientes",$array,"id",$idClient);
		

		$update =mysqli_query($conexion,$this->sql) or die ("Error al insertar cliente");


		if(!$update){

			header("Location: client.php?id=$id&succes=false"); 


		}else{
			$this->db_cliente->cerrarConexion();
	
			header("Location: client.php?id=$id&succes=true"); 

		} 
	}

	public function getDni(){
		return $this->dni;
	}

	public function getEmail(){
		return $this->email;
	}

	public function getTelefono(){
		return $this->telefono;
	}

	public function setNombres($nombres){
		$this->nombres = $nombres;
	}

	public function setApellidos($apellidos){
		$this->apellidos = $apellidos;
	}

	public function setDni($dni){
		$this->dni = $dni;
	}

	public function setEmail($email){
		$this->email = $email;
	}

	public function setTelefono($telefono){
		$this->telefono = $telefono;
	}


}

?>