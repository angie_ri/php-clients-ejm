
<!doctype html>
<html lang="en">
  
  <?php  
     include("head.php");
      ?>
  <body>

    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
      <h5 class="my-0 mr-md-auto font-weight-normal">ArSoft Develop</h5>
     <!-- <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="#">Features</a>
        <a class="p-2 text-dark" href="#">Enterprise</a>
        <a class="p-2 text-dark" href="#">Support</a>
        <a class="p-2 text-dark" href="#">Pricing</a>
      </nav>
      <a class="btn btn-outline-primary" href="#">Sign up</a>-->
    </div>

    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">Clientes</h1>
      <p class="lead">PHP-POO</p>
    </div>

    <div class="container" >
        <form method="Post" action="accions.php">
            <input type="hidden" name="form" value="getInto">
            <div class="form-group">
              <label for="nombres">Nombres</label>
              <input type="text" class="form-control" id="nombres" name="nombres" aria-describedby="" placeholder="Ingrese nombres">
            </div>
            <div class="form-group">
              <label for="inputApellido">Apellidos</label>
              <input type="text" class="form-control" id="apellido" name="apellidos" placeholder="Ingrese apellidos">
            </div>
            <div class="form-group">
              <label for="inputDNI">DNI</label>
              <input type="text" class="form-control" id="" name="dni" placeholder="Ejm 404233030">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" class="form-control" id="exampleInputEmail1"  name="email" aria-describedby="emailHelp" placeholder="Enter email">
            </div>
            <div class="form-group">
              <label for="ex1">Telefono</label>
              <input type="text" class="form-control" id="telefono"  name="telefono"  placeholder="Ingrese un telefono">
            </div>
            <button type="submit" class="btn btn-outline-dark btn-block">Enviar</button>
        </form>
        <div class="links">
          <a href="index.php" class="font-italic text-decoration-none text-left"><- Home</a>
          <a href="verClientes.php" class="font-italic text-decoration-none text-right" id="linkClient">Ver Clientes -></a>
		  </div>
           <?php  
       include("footer.php");
    ?>

    

</div>
     <?php  
     include("scripts.php");
      ?>
  </body>
</html>
