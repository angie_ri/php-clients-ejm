<!doctype html>
<html lang="es">
  <?php  
     include("head.php");
      ?>

  <body>

    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
      <h5 class="my-0 mr-md-auto font-weight-normal">ArSoft Develop</h5>
     <!-- <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="#">Features</a>
        <a class="p-2 text-dark" href="#">Enterprise</a>
        <a class="p-2 text-dark" href="#">Support</a>
        <a class="p-2 text-dark" href="#">Pricing</a>
      </nav>
      <a class="btn btn-outline-primary" href="#">Sign up</a>-->
    </div>

    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">Clientes</h1>
      <p class="lead">PHP-POO</p>
    </div>

    <div class="container" >
      <a href="verClientes.php" class="btn btn-outline-dark  btn-block " role="button" aria-pressed="true">Ver Clientes</a>
      <a href="formIngresarCliente.php" class="btn btn-outline-dark btn-block " role="button" aria-pressed="true">Ingresar Cliente</a>

     <?php  
     include("footer.php");
      ?>

</div>
   <?php  
     include("scripts.php");
      ?>
  </body>
</html>
